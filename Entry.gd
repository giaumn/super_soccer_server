extends Node

func _on_Timer_timeout():
	var rest = RoundRest.new()
	add_child(rest)
	rest.fetch_in_progress()
	var rounds = yield(rest, Constant.SIGNAL_RESPONSE_DATA)
	for r in rounds:
		var pairs = r.get('pairs')
		for pair in pairs:
			var match_data = pair.get('match')
			if match_data != null:
				continue
			
			var home = pair.get('home')
			var away = pair.get('away')
			var home_id = home.get('id')
			var away_id = away.get('id')
			
			if MatchMaker.match_by_users.get(home_id) != null or MatchMaker.match_by_users.get(away_id) != null:
				continue

			var home_formation = home.get('preferred_formation')
			var away_formation = away.get('preferred_formation')
			
			var home_model = UserModel.new()
			home_model.id = home_id
			home_model.user_name = home.get('name')
			home_model.country_code = home.get('country_code')
			home_model.team_name = home.get('team_name')
			home_model.formation = home_formation if home_formation != null else '4-3-3'
			home_model.confirmed = true
			
			var away_model = UserModel.new()
			away_model.id = away_id
			away_model.user_name = away.get('name')
			away_model.country_code = away.get('country_code')
			away_model.team_name = away.get('team_name')
			away_model.formation = away_formation if away_formation != null else '4-3-3'
			away_model.confirmed = true
			
			MatchMaker.create(r.get('league'), pair, home_model, away_model)
	remove_child(rest)
	$Timer.start()
