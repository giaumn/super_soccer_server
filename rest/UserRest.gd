extends HttpCaller

class_name UserRest

func fetch():
	.get('/me')
	var response = yield(self, Constant.SIGNAL_HTTP_SUCCESS)
	emit_signal(Constant.SIGNAL_RESPONSE_DATA, response['data'])
	
func create(data):
	.post('/users', data)
	var response = yield(self, Constant.SIGNAL_HTTP_SUCCESS)
	emit_signal(Constant.SIGNAL_RESPONSE_DATA, response['data'])

func update(data):
	.patch('/me', data)

func link(data):
	.post('/me/link', data)

func get_national_league():
	.get('/me/national_league')
	var response = yield(self, Constant.SIGNAL_HTTP_SUCCESS)
	emit_signal(Constant.SIGNAL_RESPONSE_DATA, response['data'])
