extends HttpCaller

class_name RoundRest

func fetch_in_progress():
	.get('/rounds/in_progress')
	var response = yield(self, Constant.SIGNAL_HTTP_SUCCESS)
	emit_signal(Constant.SIGNAL_RESPONSE_DATA, response['data'])
	
