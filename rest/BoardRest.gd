extends HttpCaller

class_name BoardRest

func fetch(country_code, page = 1, per = 10):
	.get('/boards/' + country_code + '?page=' + var2str(page) + '&per=' + var2str(per))
	var response = yield(self, Constant.SIGNAL_HTTP_SUCCESS)
	emit_signal(Constant.SIGNAL_RESPONSE_DATA, response['data'], response.get('paging'))
	
