extends HttpCaller

class_name BotRest

func fetch_free():
	.get('/bots/fetch_free')
	var response = yield(self, Constant.SIGNAL_HTTP_SUCCESS)
	emit_signal(Constant.SIGNAL_RESPONSE_DATA, response['data'])
	
func release(id):
	.post('/bots/' + var2str(id) + '/release', {})

func lock(id):
	.post('/bots/' + var2str(id) + '/lock', {})
