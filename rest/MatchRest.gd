extends HttpCaller

class_name MatchRest

func create(data):
	.post('/matches', data)
	var response = yield(self, Constant.SIGNAL_HTTP_SUCCESS)
	emit_signal(Constant.SIGNAL_RESPONSE_DATA, response['data'])
