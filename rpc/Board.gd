extends Node

remote func fetch(data):
	var peer_id = get_tree().get_rpc_sender_id()
	var rest = BoardRest.new()
	add_child(rest)
	rest.set_headers(['Authorization: ' + data['token']])
	rest.set_peer(peer_id)
	rest.fetch(data['country_code'], data['page'], data['per'])
	var response = yield(rest, Constant.SIGNAL_RESPONSE_DATA)
	remove_child(rest)
	rpc_id(peer_id, Constant.RPC_RET_BOARD_FETCH, response[0], response[1])
