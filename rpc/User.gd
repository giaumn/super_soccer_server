extends Node

remote func authenticate(data):
	if data['token'] == null:
		# new player
		var peer_id = get_tree().get_rpc_sender_id()
		var rest = UserRest.new()
		add_child(rest)
		rest.set_peer(peer_id)
		rest.create({'ip_address': data['ip_address']})
		var response = yield(rest, Constant.SIGNAL_RESPONSE_DATA)
		remove_child(rest)
		rpc_id(peer_id, Constant.RPC_RET_USER_AUTHENTICATE, response)
	else:
		# existing player/guest
		fetch_user(data)

remote func fetch_user(data):
	var peer_id = get_tree().get_rpc_sender_id()
	var rest = UserRest.new()
	add_child(rest)
	rest.set_peer(peer_id)
	rest.set_headers(['Authorization: ' + data['token']])
	rest.fetch()
	
	var response = yield(rest, Constant.SIGNAL_RESPONSE_DATA)
	remove_child(rest)
	
	var id = response['id']
	var matchModel = MatchMaker.reconnect(id, peer_id)
	if matchModel != null:
		response['current_match'] = matchModel.to_json()
		
	response['last_cancelation'] = Match.last_cancellation_of_users.get(id)
	
	rpc_id(peer_id, Constant.RPC_RET_USER_FETCH, response)

remote func update(data):
	var peer_id = get_tree().get_rpc_sender_id()
	var rest = UserRest.new()
	add_child(rest)
	rest.set_peer(peer_id)
	rest.set_headers(['Authorization: ' + data['token']])
	rest.update({
		'name': data.get('coach_name'), 
		'team_name': data.get('team_name'),
		'fcm_token': data.get('fcm_token')
	})
	
	var response = yield(rest, Constant.SIGNAL_HTTP_RESPONSE)
	remove_child(rest)
	rpc_id(peer_id, Constant.RPC_RET_USER_UPDATE, response)

remote func link(data):
	var peer_id = get_tree().get_rpc_sender_id()
	var rest = UserRest.new()
	add_child(rest)
	rest.set_peer(peer_id)
	rest.set_headers(['Authorization: ' + data['token']])
	rest.link({'provider': data['provider'], 'id_token': data['id_token'], 'data_action': data['action']})
	
	var response = yield(rest, Constant.SIGNAL_HTTP_RESPONSE)
	remove_child(rest)
	rpc_id(peer_id, Constant.RPC_RET_USER_LINK, response)

remote func fetch_national_league(data):
	var peer_id = get_tree().get_rpc_sender_id()
	var rest = UserRest.new()
	add_child(rest)
	rest.set_peer(peer_id)
	rest.set_headers(['Authorization: ' + data['token']])
	rest.get_national_league()
	
	var response = yield(rest, Constant.SIGNAL_RESPONSE_DATA)
	remove_child(rest)
	rpc_id(peer_id, Constant.RPC_RET_USER_FETCH_NATIONAL_LEAGUE, response)
