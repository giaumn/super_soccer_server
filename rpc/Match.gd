extends Node

var last_cancellation_of_users = {} # include both rejection and cancelation

remote func request(data):
	var last_cancelation = last_cancellation_of_users.get(data['user_id'])
	if last_cancelation != null && OS.get_unix_time() - last_cancelation <= 15:
		return
	
	# fetch player info
	var peer_id = get_tree().get_rpc_sender_id()
	var rest = UserRest.new()
	add_child(rest)
	rest.set_headers(['Authorization: ' + data['token']])
	rest.set_peer(peer_id)
	rest.fetch()
	
	var response = yield(rest, Constant.SIGNAL_RESPONSE_DATA)
	remove_child(rest)
	var user_model = UserModel.new()
	user_model.token = data['token']
	user_model.peer_id = peer_id
	user_model.id = response['id']
	user_model.user_name = response['name']
	user_model.country_code = response['country_code']
	user_model.team_name = response['team_name']
	user_model.formation = '4-3-3' if response['preferred_formation'] == null else response['preferred_formation']
	
	MatchMaker.add(user_model)
	
remote func cancel_request(data):
	last_cancellation_of_users[data['user_id']] = OS.get_unix_time()
	var peer_id = get_tree().get_rpc_sender_id()
	MatchMaker.remove(peer_id)
	rpc_id(peer_id, Constant.RPC_MATCH_SYNC_CANCEL_TIME, last_cancellation_of_users[data['user_id']])
	
remote func confirm(data):
	MatchMaker.confirm(data['user_id'], data['formation'])
	
remote func reject(data):
	last_cancellation_of_users[data['user_id']] = OS.get_unix_time()
	MatchMaker.reject(data['user_id'])
	var peer_id = get_tree().get_rpc_sender_id()
	rpc_id(peer_id, Constant.RPC_MATCH_SYNC_CANCEL_TIME, last_cancellation_of_users[data['user_id']])

remote func reconnect(data):
	var peer_id = get_tree().get_rpc_sender_id()
	MatchMaker.reconnect(data['user_id'], peer_id)
		
remote func select(data):
	var matchModel = MatchMaker.match_by_users.get(data['user_id'])
	if matchModel != null && matchModel.play_round < 4:
		if matchModel.home.id == data['user_id']:
			if matchModel.away.peer_id in Server.connected_peers:
				rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_OPPONENT_SELECT, data['turn_count'], data['selection'])
		else:
			if matchModel.home.peer_id in Server.connected_peers:
				rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_OPPONENT_SELECT, data['turn_count'], data['selection'])

remote func move(data):
	var matchModel = MatchMaker.match_by_users.get(data['user_id'])
	if matchModel != null:
		matchModel.move(data['user_id'], data['turn_count'], data['selection'])
		if matchModel.home.id == data['user_id']:
			if matchModel.away.peer_id in Server.connected_peers:
				rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_OPPONENT_CONFIRM, data['turn_count'], data['selection'])
		else:
			if matchModel.home.peer_id in Server.connected_peers:
				rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_OPPONENT_CONFIRM, data['turn_count'], data['selection'])

remote func quit(data):
	var matchModel = MatchMaker.match_by_users.get(data['user_id'])
	if matchModel != null:
		matchModel.quit(data['user_id'])
		
remote func change_formation(data):
	var matchModel = MatchMaker.match_by_users.get(data['user_id'])
	if matchModel != null:
		matchModel.change_formation(data['user_id'], data['formation'])

func create(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_RET_MATCH_REQUEST, matchModel.home.to_json(), matchModel.away.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_RET_MATCH_REQUEST, matchModel.home.to_json(), matchModel.away.to_json())

func init(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_INIT, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_INIT, matchModel.to_json())
	
func start(matchModel: MatchModel):
	MatchMaker.basic_home = null
	MatchMaker.basic_away = null
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_START, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_START, matchModel.to_json())

func new_turn(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_NEW_TURN, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_NEW_TURN, matchModel.to_json())

func skip_turn(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_SKIP_TURN, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_SKIP_TURN, matchModel.to_json())
	
func new_round(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_NEW_ROUND, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_NEW_ROUND, matchModel.to_json())
	
func end_round(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_END_ROUND, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_END_ROUND, matchModel.to_json())
	
func score(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_SCORE, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_SCORE, matchModel.to_json())

func sync(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_SYNC, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_SYNC, matchModel.to_json())
	
func cancel(target: UserModel, matchModel: MatchModel):
	if target.id == matchModel.home.id:
		if matchModel.away.peer_id in Server.connected_peers:
			rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_CANCEL)
	else:
		if matchModel.home.peer_id in Server.connected_peers:
			rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_CANCEL)

func break(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_BREAK, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_BREAK, matchModel.to_json())

func return(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_RETURN, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_RETURN, matchModel.to_json())

func end(matchModel: MatchModel):
	if matchModel.home.peer_id in Server.connected_peers:
		rpc_id(matchModel.home.peer_id, Constant.RPC_MATCH_END, matchModel.to_json())
	if matchModel.away.peer_id in Server.connected_peers:
		rpc_id(matchModel.away.peer_id, Constant.RPC_MATCH_END, matchModel.to_json())
