extends Node

var _awaiting_users = []

var matches = {}
var match_by_users = {}
var match_by_peers = {}
var basic_home = null
var basic_away = null
var match_count = 0
var _watch_timer
var _bot_rest

func _ready():
	_bot_rest = BotRest.new()
	add_child(_bot_rest)

func _find_user_index(peer_id):
	# not yet join match, find and remove user from waiting list
	var index = 0
	while index < _awaiting_users.size():
		if _awaiting_users[index].peer_id == peer_id:
			break
		index += 1

	if index < _awaiting_users.size():
		return index
	
	return -1

func destroy(matchModel):
	_release_if_bot(matchModel.home)
	_release_if_bot(matchModel.away)
	match_by_users.erase(matchModel.home.id)
	match_by_users.erase(matchModel.away.id)
	match_by_peers.erase(matchModel.home.peer_id)
	match_by_peers.erase(matchModel.away.peer_id)
	basic_away = null
	basic_home = null
	remove_child(matchModel)
				
func add(user: UserModel):
	yield (get_tree().create_timer(3), 'timeout') # wait for 3 seconds to avoid user immediately reconnect after disconnected
	_awaiting_users.push_back(user)
	
	if user.peer_id != -1:
		# only watch actual user
		_watch(user)
	
	make()
	
func remove(peer_id):
	if match_by_peers.has(peer_id):
		# no need to do anything here, since the match will be discarded automatically after timeout
		var matchModel = match_by_peers.get(peer_id)
		if matchModel.status == Constant.MATCH_STATUS_CONFIRMING:
			if matchModel.home.peer_id == peer_id:
				reject(matchModel.home.id)
			else:
				reject(matchModel.away.id)
	else:
		# not yet join match, find and remove user from waiting list
		var index = _find_user_index(peer_id)
		# found and remove from queue
		if index > -1:
			_awaiting_users.remove(index)

func reconnect(user_id, peer_id):
	if !match_by_users.has(user_id):
		return null
	
	var matchModel = match_by_users[user_id]
	var target
	
	if matchModel.home.id == user_id:
		# home reconnected
		target = matchModel.home
	else:
		# away reconnected
		target = matchModel.away
		
	# reconnect
	target.peer_id = peer_id
	match_by_peers[target.peer_id] = matchModel
	matchModel.sync()
	return matchModel

func confirm(user_id, formation):
	if !match_by_users.has(user_id):
		return
		
	var matchModel = match_by_users[user_id]
	matchModel.confirm(user_id, formation)
	if matchModel.both_confirmed():
		# start match after both confirmed
		matchModel.init()
		Match.init(matchModel)
		
func reject(user_id):
	if !match_by_users.has(user_id):
		return
		
	var matchModel = match_by_users[user_id]
	var the_other
	
	if matchModel.home.id == user_id:
		# home rejected, priority away for match making
		matchModel.cancel(matchModel.home)
		the_other = matchModel.away
	else:
		# away rejected, priority home for match making
		matchModel.cancel(matchModel.away)
		the_other = matchModel.home
	
	if the_other.peer_id != -1:
		the_other.confirmed = false
		_awaiting_users.push_front(the_other)
			
	destroy(matchModel)
	make()
	
func make():
	if _awaiting_users.size() < 2:
		return
	
	# has a pair to start match
	basic_home = _awaiting_users.pop_front()
	basic_away = _awaiting_users.pop_front()
	
	var matchModel = MatchModel.new(basic_home, basic_away)
	add_child(matchModel)
	
	# store match localy for easier retrieve
	match_by_users[basic_home.id] = matchModel
	match_by_users[basic_away.id] = matchModel
	
	if basic_home.peer_id != -1:
		match_by_peers[basic_home.peer_id] = matchModel
	
	if basic_away.peer_id != -1:
		match_by_users[basic_away.peer_id] = matchModel
	
	Match.create(matchModel)
	
	yield(get_tree().create_timer(15), 'timeout')
	
	# discard a match after 15 seconds of not confirming from both parties
	if !matchModel.is_inside_tree():
		return
		
	if matchModel.status == Constant.MATCH_STATUS_CANCELED:
		destroy(matchModel)
		return
		
	if !matchModel.both_confirmed():
		remake(matchModel)
		destroy(matchModel)

func create(league, pair, home, away):
	var matchModel = MatchModel.new(home, away)
	matchModel.league = league
	matchModel.status = Constant.MATCH_STATUS_STARTED
	matchModel.pair_id = pair.get('id')
	add_child(matchModel)
	
	# store match localy for easier retrieve
	match_by_users[home.id] = matchModel
	match_by_users[away.id] = matchModel
	
	matchModel.init()
	Match.init(matchModel)

func remake(matchModel: MatchModel):
	var remake_user
	if matchModel.home.confirmed:
		remake_user = matchModel.home
	elif matchModel.away.confirmed:
		remake_user = matchModel.away
	else:
		return
		
	if remake_user.peer_id == -1:
		# no need to remake if it is a bot
		return
	
	# priority this user for match making
	remake_user.confirmed = false
	_awaiting_users.push_front(remake_user)
	make()
	
func _watch(user):
	yield(get_tree().create_timer(7), 'timeout')
	
	if _awaiting_users.size() == 1 && _awaiting_users[0].id == user.id:
		# user is still waiting in the queue after 10s, get a free bot to serve
		_bot_rest.fetch_free()
		var response = yield(_bot_rest, Constant.SIGNAL_RESPONSE_DATA)
		var user_model = UserModel.new()
		user_model.peer_id = -1
		user_model.id = response['id']
		user_model.user_name = response['name']
		user_model.country_code = response['country_code']
		user_model.team_name = response['team_name']
		user_model.formation = '4-3-3' if response['preferred_formation'] == null else response['preferred_formation']
		MatchMaker.add(user_model)

func _release_if_bot(user):
	if user.peer_id != -1:
		return
	_bot_rest.release(int(user.id))

		
		
		
		
