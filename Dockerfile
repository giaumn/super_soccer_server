FROM ubuntu:18.04

WORKDIR /src

COPY ./build/ ./

RUN apt update
RUN apt install -y net-tools

RUN chmod a+x godot