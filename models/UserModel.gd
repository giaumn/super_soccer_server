extends Node

class_name UserModel

var token = null
var peer_id = null # -1 is bot
var id = null
var user_name = null
var team_name = null
var country_code = null
var confirmed = false
var formation = null

func to_json():
	return {
		'id': id,
		'user_name': user_name,
		'team_name': team_name,
		'country_code': country_code,
		'formation': formation
	}
