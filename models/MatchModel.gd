extends Node

class_name MatchModel

var league = null
var pair_id = null
var id = null
var home = null
var away = null
var status = null
var elapsed = 0
var current_turn = null
var turn_elapsed = 0
var home_score = 0
var away_score = 0
var home_selection = null
var away_selection = null
var ball_control = null # the current user who is having control of the ball
# There are 4 rounds: back, center, forward, gk, pass all 4 rounds to score 1 point
var play_round = 0 # 0 mean at goal keeper, increase by 1 every pass success
var current_player = 1 # player in formation who is currently controlling the ball
var match_secs = 300 # secs in real life is equals to 90 minutes in game
var wait_secs_before_started = 5
var turn_limit_secs = 8
var turn_count = 0
var break_secs = 5

var home_passes = 0
var away_passes = 0
var home_tackles = 0
var away_tackles = 0
var home_shots = 0
var away_shots = 0
var home_possession = 0.0
var away_possession = 0.0
var home_goal_saves = 0
var away_goal_saves = 0

var user_quit

var home_rewards
var away_rewards

var _turn_timer
var _match_timer
var _started = false
var _attacking = null
var _attacking_selection = null # keep the player that the attacking player has selected
var _break_timer
var _is_breaking = false
var _bot
var _home_skipped_count = 0
var _away_skipped_count = 0

func _init(_home, _away):
	add_user_signal(Constant.SIGNAL_MATCH_NEW_TURN)
	MatchMaker.match_count += 1
	id = MatchMaker.match_count
	status = Constant.MATCH_STATUS_CONFIRMING
	home = _home
	away = _away
	current_turn = home.id
	ball_control = home.id
	_attacking = home.id

	_turn_timer = Timer.new()
	add_child(_turn_timer)
	_turn_timer.wait_time = wait_secs_before_started + 2.5
	_turn_timer.connect('timeout', self, '_on_turn_timer_timeout')
	
	_match_timer = Timer.new()
	add_child(_match_timer)
	_match_timer.wait_time = 5
	_match_timer.connect('timeout', self, '_on_match_timer_timeout')
	
	_break_timer = Timer.new()
	add_child(_break_timer)
	_break_timer.wait_time = break_secs
	_break_timer.one_shot = true
	_break_timer.connect('timeout', self, '_on_break_timer_timeout')

	if home.peer_id == -1:
		_bot = Bot.new(self, home)
	elif away.peer_id == -1:
		_bot = Bot.new(self, away)
		
	if _bot != null:
		add_child(_bot)
		
func _on_turn_timer_timeout():
	if _is_breaking:
		return
		
	if _started:
		turn_elapsed += 1
		if ball_control == home.id:
			home_possession += 1
		else:
			away_possession += 1
		if turn_elapsed > turn_limit_secs:
			_process_game()
	else:
		_started = true
		yield(get_tree().create_timer(1), 'timeout')
		_start()
		
func _on_match_timer_timeout():
	elapsed += _match_timer.wait_time
	if elapsed == match_secs / 2:
		_turn_timer.stop()
		_match_timer.stop()
		yield(get_tree().create_timer(0.5), 'timeout')
		_break()
	if elapsed >= match_secs:
		# match has ended
		self.set_block_signals(true)
		_match_timer.stop()
		_turn_timer.stop()
		if _bot == null:
			_check_result()
		else:
			_bot.connect(Constant.SIGNAL_BOT_COMPLETED, self, '_on_bot_complted')
			_bot.wait_for_complete()
		
func _on_bot_complted():
	_check_result()
		
func _on_break_timer_timeout():
	_break_timer.stop()
	_return()

func _start():
	_turn_timer.stop()
	# check again after 10 seconds (10s/turn)
	home_selection = null
	away_selection = null
	status = Constant.MATCH_STATUS_STARTED
	Match.start(self)
	_turn_timer.start(1)
	_match_timer.start()
	
func _break():
	_is_breaking = true
	# check again after 10 seconds (10s/turn)
	Match.break(self)
	_break_timer.start()

func _return():
	_is_breaking = false
	Match.return(self)
	
	yield(get_tree().create_timer(4), 'timeout')
	home_selection = null
	away_selection = null
	current_turn = away.id
	ball_control = away.id
	_attacking = away.id
	current_player = 1
	play_round = 0
	turn_elapsed = 0
	Match.start(self)
	_turn_timer.start(1)
	_match_timer.start()
	emit_signal(Constant.SIGNAL_MATCH_NEW_TURN)

func _process_game():	
	_turn_timer.stop()
	turn_count += 1
	turn_elapsed = 0
	
	var selection
	var the_other
	if current_turn == home.id:
		selection = home_selection
		the_other = away
	else:
		selection = away_selection
		the_other = home

	if _attacking == current_turn:
		# one player is attacking
		if selection == null:
			# attacking, but did not play, immediately lost the ball to the other
			if _skip_match():
				return
				
			Match.skip_turn(self)
			play_round = 0
			ball_control = the_other.id
			current_turn = the_other.id
			current_player = 1 # goal keeper has the ball
			_attacking = the_other.id
			home_selection = null
			away_selection = null
			# wait a few seconds to show users ball taken/lost
			yield (get_tree().create_timer(4), 'timeout')
			Match.new_round(self)
			emit_signal(Constant.SIGNAL_MATCH_NEW_TURN)
		else:
			if play_round == 3:
				if current_turn == home.id:
					home_shots += 1
				else:
					away_shots += 1
					
			# wait to the other have played to compare result
			current_turn = the_other.id
			Match.new_turn(self)
			emit_signal(Constant.SIGNAL_MATCH_NEW_TURN)
			
		_turn_timer.start(1)
	else:
		# this turn is defending
		if selection == null:
			# defender does not select anything, the other will immediately continue on the attack
			if _skip_match():
				return
			
			Match.skip_turn(self)
			play_round += 1
			current_turn = the_other.id
			current_player = _attacking_selection
			
			yield (get_tree().create_timer(_check_score()), 'timeout')
				
			home_selection = null
			away_selection = null
			Match.new_turn(self)
			emit_signal(Constant.SIGNAL_MATCH_NEW_TURN)
			
			_turn_timer.start(1)
		else:
			# both attacker && defender have made their moves, compare result
			_compare_moves()

func _compare_moves():
	Match.end_round(self)
	if home_selection == away_selection:
		if play_round >= 3:
			if current_turn == home.id:
				home_goal_saves += 1
			else:
				away_goal_saves += 1
		else:
			if current_turn == home.id:
				home_tackles += 1
			else:
				away_tackles += 1
		
		# defending player have succussfully tackle attacking player 
		# and gain control of the ball
		# user will have the ball and start play attacking
		ball_control = current_turn
		play_round = 0
		current_player = 1
		_attacking = ball_control
		# wait a few seconds to show users success/fail pass/tackle
		yield (get_tree().create_timer(5.5), 'timeout')
	else:
		# defending player have failed to tackle attacking player
		# the same user who is currently in control of the ball
		# still have the ball and play forward, current_player is the selected player from the turn of attacking
		play_round += 1
		current_turn = ball_control
		current_player = _attacking_selection
		
		yield (get_tree().create_timer(_check_score()), 'timeout')
	
	home_selection = null
	away_selection = null
	Match.new_round(self)
	emit_signal(Constant.SIGNAL_MATCH_NEW_TURN)
	_turn_timer.start(1)

# return the number of seconds that server should wait after execution
func _check_score():
	if play_round == 4:
		# score
		if ball_control == home.id:
			home_score += 1
		else:
			away_score += 1
		
		Match.score(self)
		# wait a few seconds to show users new score status
		
		# if this user scored then the other will have ball control and new round
		play_round = 0
		if ball_control == home.id:
			ball_control = away.id
		else:
			ball_control = home.id
		current_turn = ball_control
		current_player = 1 # goal keeper has the ball
		_attacking = ball_control
		return 5
	else:
		if current_turn == home.id:
			home_passes += 1
		else:
			away_passes += 1
		# wait a few seconds to show passing animation
		Match.end_round(self)
		return 2
	
func _check_result():
	_turn_timer.stop()
	_match_timer.stop()
	_break_timer.stop()
	
	var data = {
		'pair_id': pair_id,
		'home_id': home.id,
		'away_id': away.id,
		'quit_id': user_quit,
		'home_score': home_score,
		'away_score': away_score,
		'home_passes': home_passes,
		'away_passes': away_passes,
		'home_tackles': home_tackles,
		'away_tackles': away_tackles,
		'home_shots': home_shots,
		'away_shots': away_shots,
		'home_possession': home_possession,
		'away_possession': away_possession,
		'home_goal_saves': home_goal_saves,
		'away_goal_saves': away_goal_saves,
		'home_preferred_formation': home.formation,
		'away_preferred_formation': away.formation
	}
	
	var rest = MatchRest.new()
	add_child(rest)
	rest.create(data)
	var response = yield(rest, Constant.SIGNAL_RESPONSE_DATA)
	remove_child(rest)
	
	self.home_rewards = response.get('home_rewards')
	self.away_rewards = response.get('away_rewards')
	Match.end(self)
	MatchMaker.destroy(self)

func _skip_match():
	if current_turn == home.id:
		_home_skipped_count += 1
		if _home_skipped_count >= Constant.ALLOWED_SKIPPED_TURN_COUNT:
			quit(home.id)
			return true
	else:
		_away_skipped_count += 1
		if _away_skipped_count >= Constant.ALLOWED_SKIPPED_TURN_COUNT:
			quit(away.id)
			return true
	return false

func both_confirmed():
	return home != null && home.confirmed && away != null && away.confirmed

func peer_confirmed(peer_id):
	if home.peer_id == peer_id:
		return home.confirmed
	elif away.peer_id == peer_id:
		return away.confirmed
	else:
		return false

func confirm(user_id, formation):
	if home.id == user_id:
		# home confirmed
		home.confirmed = true
		home.formation = formation
	elif away.id == user_id:
		away.confirmed = true
		away.formation = formation
		
func init():
	_turn_timer.start()
	
func sync():
	Match.sync(self)
		
func move(user_id, client_turn_count, selection):
	if _is_breaking:
		return
		
	if user_id == home.id:
		home_selection = selection
		_home_skipped_count = 0
	else:
		away_selection = selection
		_away_skipped_count = 0
	
	if _attacking == current_turn:
		_attacking_selection = selection
	
	if client_turn_count == turn_count:
		_process_game()

func cancel(canceled_user):
	if status != Constant.MATCH_STATUS_CONFIRMING:
		return
	
	status = Constant.MATCH_STATUS_CANCELED
	Match.cancel(canceled_user, self)

func quit(user_id):
	user_quit = user_id
	_check_result()

func change_formation(user_id, formation):
	if user_id == home.id:
		home.formation = formation
	else:
		away.formation = formation

func to_json():
	return {
		'id': id,
		'status': status,
		'elapsed': elapsed,
		'current_turn': current_turn,
		'turn_elapsed': turn_elapsed,
		'home_score': home_score,
		'away_score': away_score,
		'home_selection': home_selection,
		'away_selection': away_selection,
		'ball_control': ball_control,
		'play_round': play_round,
		'current_player': current_player,
		'turn_count': turn_count,
		'match_secs': match_secs,
		'break_secs': break_secs,
		'wait_secs_before_started': wait_secs_before_started,
		'turn_limit_secs': turn_limit_secs,
		'home_passes': home_passes,
		'away_passes': away_passes,
		'home_tackles': home_tackles,
		'away_tackles': away_tackles,
		'home_shots': home_shots,
		'away_shots': away_shots,
		'home_possession': home_possession,
		'away_possession': away_possession,
		'home_goal_saves': home_goal_saves,
		'away_goal_saves': away_goal_saves,
		'user_quit': user_quit,
		'home_rewards': home_rewards,
		'away_rewards': away_rewards,
		'home': home.to_json() if home != null else null,
		'away': away.to_json() if away != null else null,
		'league': league
	}
