extends Node

class_name Bot

var _DEFAULT_FORMATION = '4-3-3'
var _user
var _matchModel
var _bot_rest
var _is_processing = false

func _init(matchModel, user):
	add_user_signal(Constant.SIGNAL_BOT_COMPLETED)
	_user = user
	_matchModel = matchModel
	_bot_rest = BotRest.new()

func _ready():
	add_child(_bot_rest)
	_bot_rest.lock(int(_user.id))
	_matchModel.connect(Constant.SIGNAL_MATCH_NEW_TURN, self, '_on_new_turn')
	_confirm_after()

func _rand_selection(from, to):
	randomize()
	return range(from, to + 1)[randi() % range(from, to + 1).size()]

func _confirm_after():
	yield(get_tree().create_timer(5), 'timeout')
	MatchMaker.confirm(_user.id, _DEFAULT_FORMATION)

func _on_new_turn():
	_is_processing = true
	if _matchModel.play_round > 3:
		_is_processing = false
		emit_signal(Constant.SIGNAL_BOT_COMPLETED)
		return
		
	if _user.id != _matchModel.current_turn:
		_is_processing = false
		emit_signal(Constant.SIGNAL_BOT_COMPLETED)
		return
		
	if _matchModel.play_round == 3:
		var random_wait = _rand_selection(1, 4)
		yield(get_tree().create_timer(random_wait), 'timeout')
		
		var selection = _rand_selection(1, 4) - 1
		if selection == 0:
			selection = 'left'
		elif selection == 1:
			selection = 'center'
		else:
			selection = 'right'

		_matchModel.move(_user.id, _matchModel.turn_count, selection)
		emit_signal(Constant.SIGNAL_BOT_COMPLETED)
	else:
		var formation
		if _matchModel.ball_control == _user.id:
			formation = _user.formation
		else:
			if _matchModel.home.id == _user.id:
				formation = _matchModel.away.formation
			else:
				formation = _matchModel.home.formation
		
		# it's the bot turn, make a random move
		var formation_lines = formation.split('-')
		var play_round = _matchModel.play_round
		
		var random_start

		if play_round == 0:
			random_start = 2
		elif play_round == 1:
			random_start = int(formation_lines[0]) + 2
		else:
			random_start = int(formation_lines[0]) + int(formation_lines[1]) + 2
			
		var random_end = random_start + int(formation_lines[play_round]) - 1
		var selection = _rand_selection(random_start, random_end)

		var random_wait = _rand_selection(1, 4)
		yield(get_tree().create_timer(random_wait), 'timeout')
		
		Match.select({'user_id': _user.id, 'turn_count': _matchModel.turn_count, 'selection': selection})
		
		yield(get_tree().create_timer(3), 'timeout')
		Match.move({'user_id': _user.id, 'turn_count': _matchModel.turn_count, 'selection': selection})
		
		emit_signal(Constant.SIGNAL_BOT_COMPLETED)
	
	_is_processing = false

func wait_for_complete():
	if _is_processing:
		return
		
	emit_signal(Constant.SIGNAL_BOT_COMPLETED)
