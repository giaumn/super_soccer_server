extends Node

class_name HttpCaller

var _host = OS.get_environment('API_URL') as String
var _client = HTTPRequest.new()
var _headers = [
	'User-Agent: Super Soccer Game Server 1.0',
	'Content-Type: application/json'
]
var _peer_id

func _init():
	_host = 'http://127.0.0.1:7000' if _host == '' else _host
	add_user_signal(Constant.SIGNAL_HTTP_COMPLETED)
	add_user_signal(Constant.SIGNAL_HTTP_RESPONSE)
	add_user_signal(Constant.SIGNAL_HTTP_SUCCESS)
	add_user_signal(Constant.SIGNAL_HTTP_ERROR)
	add_user_signal(Constant.SIGNAL_RESPONSE_DATA)
	add_user_signal(Constant.SIGNAL_REQUEST_FAILED)
	
func _ready():
	add_child(_client)
	_client.connect('request_completed', self, '_on_http_request_completed')
	self.connect(Constant.SIGNAL_REQUEST_FAILED, self, '_on_request_failed')
	self.connect(Constant.SIGNAL_HTTP_ERROR, self, '_on_error')

func _on_request_failed(error_code, message):
	Server.notify_error(_peer_id, error_code, message)
	
func _on_error(error_code, message):
	if error_code >= 100 && error_code < 2000:
		# Token error
		Server.notify_error(_peer_id, error_code, message)

# Called when the HTTP request is completed.
func _on_http_request_completed(_result, response_code, __headers, body):
	# print(_result, response_code)
	emit_signal(Constant.SIGNAL_HTTP_COMPLETED, _result, response_code)
	if response_code == 200 or response_code == 422:
		var response = parse_json(body.get_string_from_utf8())
		emit_signal(Constant.SIGNAL_HTTP_RESPONSE, response)
		# print(response)
		if response['success'] == true:
			emit_signal(Constant.SIGNAL_HTTP_SUCCESS, response)
		else:
			emit_signal(Constant.SIGNAL_HTTP_ERROR, response['error_code'], response['messages'][0])
	else:
		emit_signal(Constant.SIGNAL_REQUEST_FAILED, 0, 'Oops! Something went wrong. Please try again later!')
	
func _get_request_url(path):
	return _host + '/api/v1' + path

func set_headers(headers):
	_headers.append_array(headers)

func set_peer(peer_id):
	_peer_id = peer_id
	
func get(path):
	var error = _client.request(_get_request_url(path), _headers)
	if error != OK:
		push_error('An error occurred in the HTTP request.')

func post(path, body):
	var error = _client.request(_get_request_url(path), _headers, false, HTTPClient.METHOD_POST, to_json(body))
	if error != OK:
		push_error('An error occurred in the HTTP request.')

func put(path, body):
	var error = _client.request(_get_request_url(path), _headers, false, HTTPClient.METHOD_PUT, to_json(body))
	if error != OK:
		push_error('An error occurred in the HTTP request.')
		
func patch(path, body):
	var error = _client.request(_get_request_url(path), _headers, false, HTTPClient.METHOD_PATCH, to_json(body))
	if error != OK:
		push_error('An error occurred in the HTTP request.')

func delete(path):
	var error = _client.request(_get_request_url(path), _headers)
	if error != OK:
		push_error('An error occurred in the HTTP request.')
