extends Node

var _client := PostgreSQLClient.new()

var _DB_USER = OS.get_environment('DB_USER') as String 
var _DB_PASSWORD = OS.get_environment('DB_PASSWORD') as String 
var _DB_HOST = OS.get_environment('DB_HOST') as String 
var _DB_PORT = OS.get_environment('DB_PORT') as int 
var _DB_NAME = OS.get_environment('DB_NAME') as String 

func _init():
	_DB_USER = 		'user' 						if _DB_USER == '' 		else _DB_USER
	_DB_PASSWORD = 	'password' 					if _DB_PASSWORD == '' 	else _DB_PASSWORD
	_DB_HOST = 		'localhost' 				if _DB_HOST == '' 		else _DB_HOST
	_DB_PORT = 		7432 						if _DB_PORT == 0 		else _DB_PORT
	_DB_NAME = 		'super_soccer_develop' 		if _DB_NAME == '' 		else _DB_NAME

	var _error := _client.connect('connection_established', self, '_connection_established')
	_error = _client.connect('authentication_error', self, '_authentication_error')
	_error = _client.connect('connection_closed', self, '_connection_closed')
	
	# connection to the database
	# var connection_string = 'postgresql://%s:%s@%s:%d/%s' % [_DB_USER, _DB_PASSWORD, _DB_HOST, _DB_PORT, _DB_NAME]
	# _error = _client.connect_to_host(connection_string)
	
	#if _error == OK:
		#print('Database connection started.')
	#else:
		#print('Database connection error: ', _error)

func _physics_process(_delta: float) -> void:
	_client.poll()

func _authentication_error(error_object: Dictionary) -> void:
	prints('Error connection to database:', error_object['message'])

func _connection_established():
	print('Database connection established')

func _connection_closed(clean_closure := true) -> void:
	prints('DB CLOSE,', 'Clean closure:', clean_closure)

func _exit_tree() -> void:
	_client.close()

func execute(single_sql_cmd):
	var datas := _client.execute(single_sql_cmd)
	
	if not _client.error_object.empty():
		prints('Error:', _client.error_object)
	else:
		return datas[0].data_row
