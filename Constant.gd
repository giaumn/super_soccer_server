extends Node

var ALLOWED_SKIPPED_TURN_COUNT = 7

var RPC_RET_ERROR = 'ret_error'
var RPC_RET_USER_AUTHENTICATE = 'ret_authenticate'
var RPC_RET_USER_FETCH = 'ret_fetch'
var RPC_RET_USER_UPDATE = 'ret_update'
var RPC_RET_USER_FETCH_NATIONAL_LEAGUE = 'ret_fetch_national_league'

var RPC_RET_MATCH_REQUEST = 'ret_request'

var RPC_RET_BOARD_FETCH = 'ret_fetch'

var RPC_RET_USER_LINK = 'ret_link'

var RPC_MATCH_INIT = 'init'
var RPC_MATCH_START = 'start'
var RPC_MATCH_CANCEL = 'cancel'
var RPC_MATCH_NEW_TURN = 'new_turn'
var RPC_MATCH_SKIP_TURN = 'skip_turn'
var RPC_MATCH_NEW_ROUND = 'new_round'
var RPC_MATCH_END_ROUND = 'end_round'
var RPC_MATCH_SCORE = 'score'
var RPC_MATCH_SYNC = 'sync'
var RPC_MATCH_BREAK = 'break'
var RPC_MATCH_RETURN = 'return'
var RPC_MATCH_END = 'end'
var RPC_MATCH_OPPONENT_SELECT = 'opponent_select'
var RPC_MATCH_OPPONENT_CONFIRM = 'opponent_confirm'
var RPC_MATCH_SYNC_CANCEL_TIME = 'sync_cancel_time'

var SIGNAL_HTTP_COMPLETED = 'signal_http_completed'
var SIGNAL_HTTP_RESPONSE = 'signal_http_response'
var SIGNAL_HTTP_SUCCESS = 'singal_http_success'
var SIGNAL_HTTP_ERROR = 'singal_http_error'
var SIGNAL_RESPONSE_DATA = 'signal_response_data'
var SIGNAL_REQUEST_FAILED = 'signal_request_failed'

var SIGNAL_MATCH_NEW_TURN = 'signal_match_new_turn'
var SIGNAL_BOT_COMPLETED = 'signal_bot_completed'

var MATCH_STATUS_CONFIRMING = 'confirming'
var MATCH_STATUS_STARTED = 'started'
var MATCH_STATUS_COMPLETED = 'completed'
var MATCH_STATUS_CANCELED = 'canceled'
