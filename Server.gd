extends Node

var SERVER_PORT = OS.get_environment('SERVER_PORT') as int 
var MAX_PLAYERS = OS.get_environment('MAX_PLAYERS') as int

var peer = NetworkedMultiplayerENet.new()
var key = preload('res://certificates/private.key')
var cert = load('res://certificates/public.crt')
var connected_peers = []

func _ready():
	SERVER_PORT = 7654 if SERVER_PORT == 0 else SERVER_PORT
	MAX_PLAYERS = 100 if MAX_PLAYERS == 0 else MAX_PLAYERS
	
	peer.set_dtls_certificate(cert)
	peer.set_dtls_key(key)
	peer.use_dtls = true
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	get_tree().network_peer = peer
	get_tree().connect('network_peer_connected', self, '_on_player_connected')
	get_tree().connect('network_peer_disconnected', self, '_on_player_disconnected')
	
	print('UDP server started on port ' + var2str(SERVER_PORT) + '. Max users: ' + var2str(MAX_PLAYERS))
	
func _on_player_connected(id):
	print('Client connected: ' + id as String)
	connected_peers.push_back(id)

func _on_player_disconnected(id):
	print('Client disconnected: ' + id as String)
	connected_peers.erase(id)
	MatchMaker.remove(id)
	
func notify_error(target_peer_id, error_code, message):
	print(error_code, '-', message)
	if target_peer_id in Server.connected_peers:
		rpc_id(target_peer_id, Constant.RPC_RET_ERROR, error_code, message)
